import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchAlbums} from "../store/actions/albums";
import Album from "../components/Album/Album";
import {Grid, PageHeader, Row} from "react-bootstrap";

class ArtistAlbums extends Component {
    componentDidMount () {
        console.log(this.props.match.params.artistId);
        this.props.fetchAlbums(this.props.match.params.artistId);
    }

    render() {
        return (
            <Grid>
                <Row>
                    <PageHeader>
                        Album(s)
                    </PageHeader>

                    {this.props.albums.map(album => (
                        <Album
                            key={album._id}
                            albumId={album._id}
                            title={album.title}
                            image={album.coverImage}
                            releaseYear={album.releaseYear}
                        />
                    ))}
                </Row>
            </Grid>
        )
    }
}

const mapStateToProps = state => ({
    albums: state.albums.albums
});

const mapDispatchToProps = dispatch => ({
    fetchAlbums: (artistId) => dispatch(fetchAlbums(artistId))
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistAlbums);
