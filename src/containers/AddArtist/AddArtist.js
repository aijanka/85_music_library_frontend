import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {addArtist} from "../../store/actions/artists";

class App extends Component {
    state = {
        name: '',
        photo: '',
        info: ''
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.addArtist(formData);
    };

    render() {

        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="name"
                        placeholder="Enter artist's name"
                        title="Name"
                        type="text"
                        value={this.state.name}
                        changeHandler={this.inputChangeHandler}
                        required
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="info"
                        placeholder="Enter information about the artist"
                        title="Info"
                        type="text"
                        value={this.state.info}
                        changeHandler={this.inputChangeHandler}
                        required
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="photo"
                        placeholder="Enter artist's photo"
                        title="Photo"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                        required
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
    addArtist: data => dispatch(addArtist(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
