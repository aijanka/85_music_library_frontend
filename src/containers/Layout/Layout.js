import React, {Fragment} from 'react';
import Toolbar from "../../components/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import {logoutUser} from "../../store/actions/users";

const Layout = props => (
    <Fragment>
        <header><Toolbar user={props.user} logout={props.logout}/></header>
        <main className='container'>
            {props.children}
        </main>
    </Fragment>
);

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logoutUser())
})



export default connect(mapStateToProps, mapDispatchToProps)(Layout);