import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchArtists} from "../../store/actions/artists";
import {addAlbum} from "../../store/actions/albums";

class App extends Component {
    state = {
        title: '',
        releaseYear: '',
        coverImage: '',
        artist: ''
    };

    componentDidMount() {
        this.props.fetchAlbums();
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.addTrack(formData);
    };

    render() {
        const artists = this.props.artists.map(artist => ({title: artist.name, id: artist._id}));
        console.log(artists);
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="title"
                        placeholder="Enter albums's title"
                        title="Title"
                        type="text"
                        value={this.state.title}
                        changeHandler={this.inputChangeHandler}
                        required
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="releaseYear"
                        placeholder="Enter release year of the album"
                        title="Release year"
                        type="number"
                        value={this.state.releaseYear}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="artist"
                        placeholder="Enter author of the album"
                        title="Artist"
                        type="select"
                        options={artists}
                        value={this.state.artist}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="coverImage"
                        placeholder="Add album's cover image"
                        title="Cover image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    addTrack: data => dispatch(addAlbum(data)),
    fetchAlbums: () => dispatch(fetchArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
