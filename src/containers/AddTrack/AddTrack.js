import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchAllAlbums} from "../../store/actions/albums";
import {addTrack} from "../../store/actions/tracks";

class App extends Component {
    state = {
        title: '',
        duration: '',
        album: '',
        link: ''
    };

    componentDidMount() {
        this.props.fetchAlbums();
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.addTrack(formData);
    };

    render() {
        console.log(this.props.albums);
        const albums = this.props.albums.map(album => ({title: album.title, id: album._id}));
        console.log(albums);
        return (
            <Fragment>
                <PageHeader>Add new track</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="title"
                        placeholder="Enter track's title"
                        title="Title"
                        type="text"
                        value={this.state.title}
                        changeHandler={this.inputChangeHandler}
                        required
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="link"
                        placeholder="Enter link for the track"
                        title="Link"
                        type="text"
                        value={this.state.link}
                        changeHandler={this.inputChangeHandler}
                        required
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="duration"
                        placeholder="Enter duration of the track"
                        title="Duration"
                        type="number"
                        value={this.state.duration}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="album"
                        placeholder="Enter album of the track"
                        title="Album"
                        type="select"
                        options={albums}
                        value={this.state.album}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorField('username') && this.props.error.errors.username.message}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Add</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albums.allAlbums
});

const mapDispatchToProps = dispatch => ({
    addTrack: data => dispatch(addTrack(data)),
    fetchAlbums: () => dispatch(fetchAllAlbums())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
