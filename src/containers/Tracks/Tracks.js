import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchTracks} from "../../store/actions/tracks";
import Track from "../../components/Track/Track";
import {Grid} from "react-bootstrap";

class Tracks extends Component {
    componentDidMount() {
        console.log(this.props);
        const urlParams = new URLSearchParams(this.props.location.search);
        const p = urlParams.get('album');
        this.props.fetchTracks(p);
    }

    render() {
        return (
            <Grid>
                {this.props.tracks.map(track => (
                    <Track
                        key={track._id}
                        number={track.number}
                        title={track.title}
                    />
                ))}
            </Grid>

        );
    }
}

const mapStateToProps = state => ({
    tracks: state.tracks.tracks
});

const mapDispatchToProps = dispatch => ({
    fetchTracks: id => dispatch(fetchTracks(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);
