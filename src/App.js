import React, {Component} from 'react';
import Layout from "./containers/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import {Switch, Route} from 'react-router-dom';
import ArtistAlbums from "./containers/ArtistAlbums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

class App extends Component {
    render() {
        return (
            <Layout>

                <Switch>
                    <Route path="/" exact component={Artists}/>
                    <Route path="/albums/artistId/:artistId" exact component={ArtistAlbums}/>
                    <Route path="/tracks" component={Tracks}/>
                    <Route path='/register' exact component={Register}/>
                    <Route path='/login' exact component={Login}/>
                    <Route path='/addArtist' exact component={AddArtist}/>
                    <Route path='/addAlbum' exact component={AddAlbum}/>
                    <Route path='/addTrack' exact component={AddTrack}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
