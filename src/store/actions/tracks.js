import axios from 'axios';
import {FETCH_HISTORY_SUCCESS, FETCH_TRACKS_SUCCESS} from "./actionTypes";
import {push} from 'react-router-redux';

const fetchTracksSuccess = (tracks) => ({type: FETCH_TRACKS_SUCCESS, tracks});

export const fetchTracks = (id) => {
    return dispatch => {
        axios.get('/tracks?album=' + id).then(response => {
            dispatch(fetchTracksSuccess(response.data))
        })
    }
};

export const fetchHistorySuccess = (history) => {
    return {type: FETCH_HISTORY_SUCCESS, history};
};

export const fetchHistory = (token) => {
    return (dispatch) => {
        return axios.get('/history', {headers: {"Token": token}})
            .then(response => {
                dispatch(fetchHistorySuccess(response.data))
            })
    }
};

export const addTrack = track => {
    return (dispatch, getState) => {
        const headers = {'Token': getState().users.user.token};
        axios.post('/tracks', track, {headers}).then(response => {
            dispatch(push('/'));
        })
    }
}