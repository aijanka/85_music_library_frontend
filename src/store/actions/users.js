import axios from 'axios';
import {push} from 'react-router-redux';
import {
    LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_SUCCESS, REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "./actionTypes";

export const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS};
};

export const registerUserFailure = error => {
    return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = (userData) => {
    return dispatch => {
        axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess());
                dispatch(push('/'));
            },
            error => {
                dispatch(registerUserFailure(error.response.data));
            }
        )
    };
};

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                dispatch(push('/'));
            }, error => {
                const errorObj = error.response ? error.response.data : {error: 'No internet'};
                dispatch(loginUserFailure(errorObj));
            }
        )
    }
};

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = {Token: getState().users.user.token};
        axios.delete('/users/sessions', {token}).then(
            response => {
                dispatch({type: LOGOUT_SUCCESS});
                dispatch(push('/'));
            }, error => {
                console.log('error in logging out');
            }
        )
    }
};



