import axios from 'axios';
import {FETCH_ARTISTS_SUCCESS} from "./actionTypes";
import {push} from 'react-router-redux';

export const fetchArtistsSuccess = (artists) => {
    return {type: FETCH_ARTISTS_SUCCESS, artists}
};

export const fetchArtists = () => {
    return (dispatch) => {
        axios.get('/artists').then(
            response => {
                dispatch(fetchArtistsSuccess(response.data));
            }

        )
    }
};


export const addArtist = data => {
    return (dispatch , getState)=> {
        const headers = {'Token': getState().users.user.token}
        axios.post('/artists', data, {headers}).then(
            response => dispatch(push('/'))
        )
    }
}