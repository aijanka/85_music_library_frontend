import axios from 'axios';
import {FETCH_ALBUMS_ALL_SUCCESS, FETCH_ALBUMS_SUCCESS} from "./actionTypes";
import {push} from 'react-router-redux';

export const fetchAlbumsSuccess = (albums) => {
    return {type: FETCH_ALBUMS_SUCCESS, albums}
};

export const fetchAlbums = (artistId) => {
    return (dispatch) => {
        axios.get('/albums/artistId/' + artistId).then(
            response => {
                console.log(response.data);
                dispatch(fetchAlbumsSuccess(response.data));
            }

        )
    }
};

const fetchAllAlbumsSuccess = allAlbums => ({type: FETCH_ALBUMS_ALL_SUCCESS, allAlbums});


export const fetchAllAlbums = () => {
    return (dispatch) => {
        axios.get('/albums').then(
            response => {
                dispatch(fetchAllAlbumsSuccess(response.data));
            }

        )
    }
};

export const addAlbum = album => {
    return (dispatch, getState) => {
        const headers = {'Token': getState().users.user.token};
        axios.post('/albums', album, {headers}).then(response => {
            dispatch(push('/'));
        })
    }
}