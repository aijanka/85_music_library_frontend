import React, {Fragment} from 'react';
import { NavItem} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap';

const AnonymousMenu = props => (
    <Fragment>
        <LinkContainer to="/register" exact>
            <NavItem>Register</NavItem>
        </LinkContainer>

        <LinkContainer to="/login" exact>
            <NavItem>Login</NavItem>
        </LinkContainer>
    </Fragment>
);

export default AnonymousMenu;