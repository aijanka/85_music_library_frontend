import React from 'react';
import {Button, Col, Thumbnail} from "react-bootstrap";

import notFound from '../../assets/images/Image-not-found.gif';
import {LinkContainer} from "react-router-bootstrap";

const Artist = props => {
    let image = notFound;
    if(props.image) {
    image = 'http://localhost:8000/uploads/' + props.image;
    }

    return (
        <Col xs={6} md={4}>
            <Thumbnail src={image} alt={props.name}>
                <h3>{props.name}</h3>
                <LinkContainer to={'/albums/artistId/' + props.id} exact><Button bsStyle="default">View more</Button></LinkContainer>
            </Thumbnail>
        </Col>
    )
};

export default Artist;