import React from 'react';
import {Button, Col, Thumbnail} from "react-bootstrap";

import notFound from '../../assets/images/Image-not-found.gif';
import {LinkContainer} from "react-router-bootstrap";

const Album = props => {
    let image = notFound;
    if(props.image) {
        image = 'http://localhost:8000/uploads/' + props.image;
    }

    return (
        <Col xs={6} md={4}>
            <Thumbnail src={image} alt={props.title}>
                <h3>{props.title}</h3>
                <p>Year of release: {props.releaseYear}</p>
                <LinkContainer to={'/tracks?album=' + props.albumId} exact><Button bsStyle="default">View all tracks</Button></LinkContainer>
            </Thumbnail>
        </Col>
    )
};

export default Album;