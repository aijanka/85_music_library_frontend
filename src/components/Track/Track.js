import React from 'react';
import {Panel} from "react-bootstrap";

const Track = props => (
    <Panel>
        <Panel.Body>{props.number ? props.number + '.' : null} {props.title}</Panel.Body>
    </Panel>
);

export default Track;